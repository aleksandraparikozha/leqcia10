package com.example.leqcia10

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_second.*

class SecondAcitivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        glide()
    }

    private fun glide(){
        Glide.with(this).load("https://static.designboom.com/wp-content/uploads/2018/09/photos-burning-man-2018-designboom-1.jpg").into(image1)
        Glide.with(this).load("https://static.designboom.com/wp-content/uploads/2018/09/photos-burning-man-2018-designboom-2.jpg").into(image2)
        Glide.with(this).load("https://static.designboom.com/wp-content/uploads/2018/09/photos-burning-man-2018-designboom-28.jpg").into(image3)

    }
}